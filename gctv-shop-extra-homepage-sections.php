<?php
/**
 * Plugin Name: GCTV Shop Extra Homepage Sections
 * Plugin URI: https://bitbucket.org/grantcardone/gctv-shop-extra-homepage-sections
 * Description: Adds extra sections to the homepage to list products from specific categories.
 * Version: 1.0.0
 * Author: Elvis Morales
 * Author URI: http://elvismdev.io/
 * Requires at least: 3.8.1
 * Tested up to: 4.4.2
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

// Display Webcasts Products
if ( ! function_exists( 'gctvsehs_webinars_products' ) ) {
	function gctvsehs_webinars_products( $args ) {

		if ( is_woocommerce_activated() ) {

			$args = apply_filters( 'gctvsehs_webinars_products_args', array(
				'limit' 			=> 4,
				'columns' 			=> 4,
				'orderby'			=> 'date',
				'order'				=> 'desc',
				'title'				=> __( 'Webinars', 'storefront' ),
				'category'			=> 'on-demand-webcasts',	// On-Demand Webcasts
				) );

			echo '<section class="storefront-product-section gctvsehs-webinars-products">';

			do_action( 'gctvsehs_homepage_before_webinars_products' );

			echo '<h2 class="section-title">' . wp_kses_post( $args['title'] ) . '</h2>';

			do_action( 'gctvsehs_homepage_after_webinars_products_title' );

			echo storefront_do_shortcode( 'recent_products',
				array(
					'per_page' 	=> intval( $args['limit'] ),
					'columns'	=> intval( $args['columns'] ),
					'orderby'	=> esc_attr( $args['orderby'] ),
					'order'		=> esc_attr( $args['order'] ),
					'category'	=> esc_attr( $args['category'] ),
					) );

			do_action( 'gctvsehs_homepage_after_webinars_products' );

			echo '</section>';

		}
	}
}

// Display Packages Products
if ( ! function_exists( 'gctvsehs_packages_products' ) ) {
	function gctvsehs_packages_products( $args ) {

		if ( is_woocommerce_activated() ) {

			$args = apply_filters( 'gctvsehs_packages_products_args', array(
				'limit' 			=> 4,
				'columns' 			=> 4,
				'orderby'			=> 'date',
				'order'				=> 'desc',
				'title'				=> __( 'Packages', 'storefront' ),
				'category'			=> 'packages',	// Packages
				) );

			echo '<section class="storefront-product-section gctvsehs-packages-products">';

			do_action( 'gctvsehs_homepage_before_packages_products' );

			echo '<h2 class="section-title">' . wp_kses_post( $args['title'] ) . '</h2>';

			do_action( 'gctvsehs_homepage_after_packages_products_title' );

			echo storefront_do_shortcode( 'recent_products',
				array(
					'per_page' 	=> intval( $args['limit'] ),
					'columns'	=> intval( $args['columns'] ),
					'orderby'	=> esc_attr( $args['orderby'] ),
					'order'		=> esc_attr( $args['order'] ),
					'category'	=> esc_attr( $args['category'] ),
					) );

			do_action( 'gctvsehs_homepage_after_packages_products' );

			echo '</section>';

		}
	}
}

// Display eBooks Products
if ( ! function_exists( 'gctvsehs_ebooks_products' ) ) {
	function gctvsehs_ebooks_products( $args ) {

		if ( is_woocommerce_activated() ) {

			$args = apply_filters( 'gctvsehs_ebooks_products_args', array(
				'limit' 			=> 4,
				'columns' 			=> 4,
				'orderby'			=> 'date',
				'order'				=> 'desc',
				'title'				=> __( 'eBooks', 'storefront' ),
				'category'			=> 'ebooks',	// eBooks
				) );

			echo '<section class="storefront-product-section gctvsehs-ebooks-products">';

			do_action( 'gctvsehs_homepage_before_ebooks_products' );

			echo '<h2 class="section-title">' . wp_kses_post( $args['title'] ) . '</h2>';

			do_action( 'gctvsehs_homepage_after_ebooks_products_title' );

			echo storefront_do_shortcode( 'recent_products',
				array(
					'per_page' 	=> intval( $args['limit'] ),
					'columns'	=> intval( $args['columns'] ),
					'orderby'	=> esc_attr( $args['orderby'] ),
					'order'		=> esc_attr( $args['order'] ),
					'category'	=> esc_attr( $args['category'] ),
					) );

			do_action( 'gctvsehs_homepage_after_ebooks_products' );

			echo '</section>';

		}
	}
}

// Add the sections to the homepage action hook of Storefront Theme.
add_action( 'homepage', 'gctvsehs_webinars_products' );
add_action( 'homepage', 'gctvsehs_packages_products' );
add_action( 'homepage', 'gctvsehs_ebooks_products' );


?>
